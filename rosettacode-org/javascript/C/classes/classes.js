
class Car {
  
  static brands = ["Mazda", "Volva"];

  weight = 1000;

  brand;

  price;

  constructor(brand, weight) {
    if(brand) this.brand = brand;
    if(weight) this.weight = weight 
  }


  drive(distance=10) {
    console.log(`A ${this.brand} ${this.constructor.name} drove ${distance}cm`);
  }

  get formattedStats() {

    let out =
      `Type: ${this.constructor.name.toLowerCase()}`
      + `\nBrand: ${this.brand}`
      + `\nWeight: ${this.weight}`;

    if(this.size) out += `\nSize: ${this.size}`;

    return out;
  }

}

class Truck extends Car {
  size;

  constructor(brand, size) {
    super(brand, 2000);
    if(size) this.size = size;
  }
}

let myTruck = new Truck("Volvo", 2);
console.log(myTruck.formattedStats);
myTruck.drive(40);

