
const results = (() => {
    'use strict';

    let allEqual = xs => and(zipWith(equal, xs, xs.slice(1))),
    
        azSorted = xs => and(zipWith(azBefore, xs, xs.slice(1))),

        equal = (a, b) => a === b,

        azBefore = (a, b) => a.toLowerCase() <= b.toLowerCase();


    let and = xs => xs.reduceRight((a, x) => a && x, true),

        zipWith = (f, xs, ys) => {
            let ny = ys.length;
            return (xs.length <= ny ? xs : xs.slice(0, ny))
                .map((x, i) => f(x, ys[i]));
        };

        let lists = [
            ['isiZulu', 'isiXhosa', 'isiNdebele', 'Xitsonga',
                'Tshivenda', 'Setswana', 'Sesotho sa Leboa', 'Sesotho',
                'English', 'Afrikaans'
            ],
            ['Afrikaans', 'English', 'isiNdebele', 'isiXhosa',
                'isiZulu', 'Sesotho', 'Sesotho sa Leboa', 'Setswana',
                'Tshivenda', 'Xitsonga',
            ],
            ['alpha', 'alpha', 'alpha', 'alpha', 'alpha', 'alpha',
                'alpha', 'alpha', 'alpha', 'alpha', 'alpha', 'alpha'
            ]
        ];
    
        return {
            allEqual: lists.map(allEqual),
            azSorted: lists.map(azSorted)
        };
})();

console.log(results);