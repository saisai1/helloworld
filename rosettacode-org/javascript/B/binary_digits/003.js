(() => {
  "use strict";

  const showIntAtBase = base => 
    toChr => n => rs => {
      const go = ([x, d], r) => {
        const r_ = toChr(d) + r;

        return 0 !== x ? (
          go(quotRem(x)(base), r_)
        ) : r_;
      }
    

  const e = "error: showIntAtBase applied to";

  return 1 >= base ? (
    `${e} unsupported base`
  ) : 0 > n ? (
    `${e} negative number`
  ) : go(quotRem(n)(base), rs);

 };
  
 const main = () => {
   const showHanBinary = n => 
     showIntAtBase(2)(
      x => "0--" [x]
     )(n)("");

   return [5, 50, 9000]
    .map(
      n => `${n} -> ${showHanBinary(n)}`
    )
    .join("\n");
  
  };

  const quotRem = m => 
    n => [Math.trunc(m / n), m % n];

  return main();

})();
